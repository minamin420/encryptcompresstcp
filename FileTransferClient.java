/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filetransferclient;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.zip.InflaterInputStream;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.SecretKeySpec;
/**
 *
 * @author ASUS
 */
public class FileTransferClient {

    /**
     * @param args the command line arguments
     */
   //private Socket s;
	
   /*private static final String ALGORITHM = "AES";
   private static final String TRANSFORMATION = "AES";
*/
   
        
	/*public FileTransferClient(String host, int port, File file) {
		try {
			s = new Socket(host, port);
			sendFile(file);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}*/
	
        public static void decrypt(String key, File file, File decryptFile) throws CryptoException, IllegalBlockSizeException, BadPaddingException, NoSuchPaddingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, InvalidKeySpecException{
            crypt(Cipher.DECRYPT_MODE, key, file, decryptFile);
        }
        
	/*public void sendFile(File file) throws IOException {
            
                DataOutputStream dos = new DataOutputStream(s.getOutputStream());
		FileInputStream fis = new FileInputStream(file);
		byte[] buffer = new byte[4096];
		while (fis.read(buffer) > 0) {
			dos.write(buffer);
		}
		
		fis.close();
		dos.close();
            
			
	}*/
        
	public static void crypt(int cipherMode, String cipherKey, File file, File decryptFile) throws CryptoException, IllegalBlockSizeException, BadPaddingException, NoSuchPaddingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, InvalidKeySpecException {
            try{
             Key secretKey = new SecretKeySpec (cipherKey.getBytes(), "AES");
             Cipher cipher = Cipher.getInstance("AES");
             cipher.init(cipherMode, secretKey);
             FileInputStream fis = new FileInputStream(file);
             byte[] inbyte = new byte[4096];
             fis.read(inbyte);
             byte[] outbyte = cipher.doFinal(inbyte);
             FileOutputStream fos = new FileOutputStream(decryptFile);
             fos.write(outbyte);
             
             fos.flush();
             fis.close();
             fos.close();
            } catch (InvalidKeyException | IOException ex) {
                throw new CryptoException("Error decrypting file", ex);
            }
        }
        
	public static void main(String[] args) throws IOException, IllegalBlockSizeException, BadPaddingException, NoSuchPaddingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, InvalidKeySpecException {
            Socket s;
            FileInputStream fis;
            FileOutputStream fos;
            FileOutputStream fosr;
            DataInputStream dis;
            InflaterInputStream iis;
            
            String key = "Asmddweqweoiasdp";
            File outputFile = new File("qwea.rar");
            File decryptedFile = new File("qwea.decrypted");
            File decompressFile = new File("qwea.encrypted");
            
            try{
                s = new Socket(InetAddress.getLocalHost(), 5000);
                System.out.println("Connected to server");
                dis = new DataInputStream(s.getInputStream());
                fos = new FileOutputStream(outputFile);
                byte[] buffer = new byte[4096];
                    int filesize = 3048570;
                    int read = 0;
                    int totalRead = 0;
                    int remaining = filesize;
                    while((read = dis.read(buffer, 0, Math.min(buffer.length, remaining))) > 0) {
			totalRead += read;
			remaining -= read;
			System.out.println("read " + totalRead + " bytes.");
			fos.write(buffer, 0, read);
		}
                
                fis = new FileInputStream(outputFile);
                fosr = new FileOutputStream(decompressFile);
                iis = new InflaterInputStream(fis);
                
                int data;
                while((data=iis.read())!=-1){
                    System.out.println("Decompressing File");
                    fosr.write(data);
                }
                
                fosr.close();
                iis.close();
                
                FileTransferClient.decrypt(key, decompressFile, decryptedFile);
                /*byte[] buffer = new byte[4096];
		while (fis.read(buffer) > 0) {
			dos.write(buffer);
		}*/
                fos.close();
                dis.close();
            } catch (CryptoException | IOException e){
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
	}

    public static class CryptoException extends Exception {

        public CryptoException() {
        }
        
        public CryptoException(String message, Throwable throwable) {
        super(message, throwable);
    }
    }
}
