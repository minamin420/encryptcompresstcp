/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filetransferserver;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Random;
import java.util.zip.DeflaterOutputStream;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author ASUS
 */
public class FileTransferServer extends Thread{

    /**
     * @param args the command line arguments
     */
   
	/*public FileTransferServer(int port) {
		try {
			ss = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void run() {
		while (true) {
			try {
				Socket clientSock = ss.accept();
				saveFile(clientSock);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void saveFile(Socket clientSock) throws IOException {
		DataInputStream dis = new DataInputStream(clientSock.getInputStream());
		FileOutputStream fos = new FileOutputStream("testfile.jpg");
		byte[] buffer = new byte[4096];
		
		int filesize = 3048570; // Send file size in separate msg
		int read = 0;
		int totalRead = 0;
		int remaining = filesize;
		while((read = dis.read(buffer, 0, Math.min(buffer.length, remaining))) > 0) {
			totalRead += read;
			remaining -= read;
			System.out.println("read " + totalRead + " bytes.");
			fos.write(buffer, 0, read);
		}
		
		fos.close();
		dis.close();
	}*/
	
    public static void encrypt(String key, File file, File encryptFile) throws CryptoException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, InvalidKeyException, InvalidAlgorithmParameterException {
        crypt(Cipher.ENCRYPT_MODE, key, file, encryptFile);
    }
    
    public static void crypt(int cipherMode, String cipherKey, File file, File encryptFile) throws CryptoException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, InvalidKeyException, InvalidAlgorithmParameterException {
            try{
             Key secretKey = new SecretKeySpec (cipherKey.getBytes(), "AES");
             Cipher cipher = Cipher.getInstance("AES");
             cipher.init(cipherMode, secretKey);
             FileInputStream fis = new FileInputStream(file);
             byte[] inbyte = new byte[4096];
             fis.read(inbyte);
             byte[] outbyte = cipher.doFinal(inbyte);
             FileOutputStream fos = new FileOutputStream(encryptFile);
             fos.write(outbyte);
            
             fos.flush();
             fis.close();
             fos.close();
            } catch (NoSuchPaddingException | NoSuchAlgorithmException
                | IOException ex) {
                throw new CryptoException("Error encrypting file", ex);
            }
        }
    
	public static void main(String[] args) throws IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, InvalidKeyException, InvalidAlgorithmParameterException {
            Socket s;
            DataOutputStream dos;
            FileInputStream fis;
            FileOutputStream fos;
            DeflaterOutputStream Dos;
            
            String key = "Asmddweqweoiasdp";
            File inputFile = new File("qweqweqweqwe.jpg");
            File encryptedFile = new File("qwea.encrypted");
            
            try{
                FileTransferServer.encrypt(key, inputFile, encryptedFile);
                System.out.println("Encrypting data");
                
                FileInputStream fisr = new FileInputStream(encryptedFile);
                fos = new FileOutputStream("qwea.rar");
                Dos = new DeflaterOutputStream(fos);
                
                int data;
                while((data=fisr.read())!=1){
                    System.out.println("Compressing data");
                    Dos.write(data);
                }
                
                fisr.close();
                Dos.close();
                
                ServerSocket ss = new ServerSocket(5000);
                
                while(true){
                    s = ss.accept();
                    dos = new DataOutputStream(s.getOutputStream());
                    fis = new FileInputStream("qwea.rar");
                    /*byte[] buffer = new byte[4096];
                    int filesize = 3048570; // Send file size in separate msg
                    int read = 0;
                    int totalRead = 0;
                    int remaining = filesize;
                    while((read = dis.read(buffer, 0, Math.min(buffer.length, remaining))) > 0) {
			totalRead += read;
			remaining -= read;
			System.out.println("read " + totalRead + " bytes.");
			fos.write(buffer, 0, read);
		}*/
                    byte[] buffer = new byte[4096];
                    while (fis.read(buffer) > 0) {
			dos.write(buffer);
                    }
                    fis.close();
                    dos.close();
                }
               
            } catch (CryptoException | IOException e){
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
	}
        
        public static class CryptoException extends Exception {

        public CryptoException() {
        }
        
        public CryptoException(String message, Throwable throwable) {
        super(message, throwable);
    }
    }
}

